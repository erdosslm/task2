INSERT INTO fond (fond_number,created_timestamp,created_by,updated_timestamp,updated_by)
VALUES
    ('+77478374565',1,1,1,1),
    ('+77478374565',2,1,2,3),
    ('+77478374565',3,3,3,3);

INSERT INTO company (name_ru,name_kz,name_en,bin,parent_id,fond_id,created_timestamp,created_by,updated_timestamp,updated_by)
VALUES
    ('aaa','aaa','aaa','aaa',1,2,3,4,5,6),
    ('bbb','bbb','bbb','bbb',1,2,3,4,5,6),
    ('ccc','ccc','ccc','ccc',1,2,3,4,5,6);

INSERT INTO company_unit (name_ru,name_kz,name_en,parent_id,year,company_id,code_index,created_timestamp,created_by,updated_timestamp,updated_by)
VALUES
    ('aaa','aaa','aaa',1,2,3,'123',4,5,6,7),
    ('bbb','bbb','bbb',1,2,3,'123',4,5,6,7),
    ('ccc','ccc','ccc',1,2,3,'123',4,5,6,7);

INSERT INTO users (auth_id,name,fullname,surname,secondname,status,company_unit_id,password,last_login_timestamp,iin,is_active,is_activeted,company_id,code_index,created_timestamp,created_by,updated_timestamp,updated_by)
VALUES
    (1,'aaa','aaa','aaa','aaa','aaa',2,'aaa',3,'aaa',true,true,4,'aaa',5,6,7,8),
    (1,'bbb','bbb','bbb','bbb','bbb',2,'bbb',3,'bbb',true,true,4,'bbb',5,6,7,8),
    (1,'ccc','ccc','ccc','ccc','ccc',2,'ccc',3,'ccc',true,true,4,'ccc',5,6,7,8);

INSERT INTO auth (username,email,password,rope,forgot_password_key,forgot_password_key_timestamp,company_unit_id)
VALUES
    ('aaa','aaa','aaa','aaa','aaa',1,2),
    ('bbb','bbb','bbb','bbb','bbb',1,2),
    ('ccc','ccc','ccc','ccc','ccc',1,2);

INSERT INTO record (number,type,company_unit_id,created_timestamp,created_by,updated_timestamp,updated_by)
VALUES
    ('aaa','aaa',1,2,3,4,5),
    ('bbb','bbb',1,2,3,4,5),
    ('ccc','ccc',1,2,3,4,5);

INSERT INTO case_index (case_index,title_ru,title_kx,title_en,storage_type,storage_year,note,company_unit_id,nomenclature_id,created_timestamp,created_by,updated_timestamp,updated_by)
VALUES
    ('aaa','aaa','aaa','aaa',1,2,'aaa',3,4,5,6,7,8),
    ('bbb','bbb','bbb','bbb',1,2,'bbb',3,4,5,6,7,8),
    ('ccc','ccc','ccc','ccc',1,2,'ccc',3,4,5,6,7,8);

INSERT INTO nomenclature (nomenclature_number,year,nomenclature_summary_id,company_unit_id,created_timestamp,created_by,updated_timestamp,updated_by)
VALUES
    ('aaa',1,2,3,4,5,6,7),
    ('bbb',1,2,3,4,5,6,7),
    ('ccc',1,2,3,4,5,6,7);

INSERT INTO case_table (case_number,volume_number,case_title_ru,case_title_kz,case_title_en,start_date,end_date,page_number,signature_flag_eds,signature_eds,naf_sign_sending,deletion_sign,restricted_flag,hash,version,id_versoin,active_version_flag,note,location_id,index_case_id,record_id,destruction_act_id,structural_unit_id,blockchain_address_case,blockchain_date_added,created_timestamp,created_by,updated_timestamp,updated_by)
VALUES
    ('aaa','aaa','aaa','aaa','aaa',1,2,3,true,'aaa',true,true,true,'aaa',4,'aaa',true,'aaa',5,6,7,8,9,'aaa',10,12,13,14,15),
    ('bbb','bbb','bbb','bbb','bbb',1,2,3,true,'bbb',true,true,true,'bbb',4,'bbb',true,'bbb',5,6,7,8,9,'bbb',10,12,13,14,15),
    ('ccc','ccc','ccc','ccc','ccc',1,2,3,true,'ccc',true,true,true,'ccc',4,'ccc',true,'ccc',5,6,7,8,9,'ccc',10,12,13,14,15);

INSERT INTO request( request_user_id, response_user_id, case_id, case_index_id, created_type, comment, status, timestamp, sharestamp, sharefinish, favorite, updated_timestamp, updated_by, declinenote, company_unit_id, from_request_id)
VALUES
    (1,2,3,4,'aaa','aaa','aaa',5,6,7,true,8,9,'aaa',10,11),
    (1,2,3,4,'bbb','bbb','bbb',5,6,7,true,8,9,'bbb',10,11),
    (1,2,3,4,'ccc','ccc','ccc',5,6,7,true,8,9,'ccc',10,11);

INSERT INTO share (request_id,note,sender_id,receiver_id,share_timestamp)
VALUES
    (1,'aaa',2,3,4),
    (1,'bbb',2,3,4),
    (1,'ccc',2,3,4);

INSERT INTO status_request_history( request_id, status, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES (1,'aaa',1,1,1,1),
       (2,'bbb',2,2,2,2),
       (3,'ccc',3,3,3,3);
INSERT INTO destruction_act(act_number,base,structural_subdivision_id,created_timestamp,created_by,updated_timestamp,updated_by)
VALUES ('01','01',1,1,1,1,1),
       ('02','02',2,2,2,2,2),
       ('03','03',3,3,3,3,3);

INSERT INTO nomenclature_summary(number,year,company_unit_id,created_timestamp,created_by,updated_timestamp,updated_by)
VALUES  ('01',1,1,1,1,1,1),
        ('02',2,2,2,2,2,2),
        ('03',3,3,3,3,3,3);

INSERT INTO catalog_case(case_id, catalog_id, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES (1,1,1,1,1,1,1),
       (2,2,2,2,2,2,2),
       (3,3,3,3,3,3,3);

INSERT INTO search_key_routing(search_key_id, table_name, table_id, type)
VALUES (1,'01',1,'01'),
       (2,'02',2,'02'),
       (3,'03',3,'03');

INSERT INTO searchkey( name, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES ('01',1,1,1,1,1),
       ('02',2,2,2,2,2),
       ('03',3,3,3,3,3);

INSERT INTO file(name, type, size, page_count, hash, is_deleted, file_binary_id, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES ('01','01',1,1,'01',true,3,1,1,1,1),
       ('02','02',2,2,'02',false,2,2,2,2,2),
       ('03','03',3,3,'03',true,3,3,3,3,3);

INSERT INTO tepfiles(file_binary, file_binary_byte)
VALUES ('01','1'),
       ('02','2'),
       ('03','3');

INSERT INTO file_routing( file_id, table_name, table_id, type)
VALUES (1,'01',1,'01'),
       (2,'02',2,'02'),
       (3,'03',3,'03');

INSERT INTO location( row, line, location_column, box, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES ('01','01','01','01',1,1,1,1,1),
       ('02','02','02','02',2,2,2,2,2),
       ('03','03','03','03',3,3,3,3,3);

INSERT INTO notification(object_type, object_id, company_unit_id, user_id, created_timestamp, viewed_timestamp, is_viewed, title, text, company_id)
VALUES ('01',1,1,1,1,1,1,'01','01',1),
       ('02',2,2,2,2,2,2,'02','02',2),
       ('03',3,3,3,3,3,3,'03','03',3);

INSERT INTO catolog_table(name_ru, name_kz, name_en, parent_id, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES ('01','01','01',1,1,1,1,1,1),
       ('02','02','02',2,2,2,2,2,2),
       ('02','02','02',3,3,3,3,3,3);

INSERT INTO activate_journal( event_type, object_type, object_id, created_timestamp, created_by, message_level, message)
VALUES ('01','01',1,1,1,'01','01'),
       ('02','02',2,2,2,'02','02'),
       ('03','03',3,3,3,'03','03');
